<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\Notifications;
class Notification extends Controller
{
    public function index()
    {

    	event(new Notifications("Hi this is my first message with pusher"));

    	echo "Event Fired!";

    }
}
